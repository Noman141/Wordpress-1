<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpdb');

/** MySQL database username */
define('DB_USER', 'noman');

/** MySQL database password */
define('DB_PASSWORD', 'v7Edm1w1hcBmo3EI');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#wD/@i.ahL6g).Gg(;qG[4q^R(`Z#Zph(yi2#W4@:<0*>u!/F^Y+mu@*BN2ay4:(');
define('SECURE_AUTH_KEY',  '=ka6xp8 UP@-,F|D^6@4Pjhek*/*/6m>EVPG[Y;7)<}`H;+m9DD^ b%4m+nc~Fgy');
define('LOGGED_IN_KEY',    '`PEj&Z;lS(]`eAMG,}ThKuTmwjrF#6?$yEyn,3.%06>LKI0Py>k5l09l9J))@=M:');
define('NONCE_KEY',        '|D2(LaHB_~-:ezfo>Vbu0QlSb+5`c._I_K{+H`0alRKUnW[+8-S0H5MQkhV~la67');
define('AUTH_SALT',        ' q&?2@W.82EU8PDrTq.lJ~_&&yvGQa+6Mdor?*4/$lzT/F_>LW[l`9!?.a{GuY<H');
define('SECURE_AUTH_SALT', '{1A:?zD3Ttr,nX4j}?_4u2zU.lXzyc!dNgA3 QJ}+QQ4{Y,c 1jd&1t(r#:}?BGV');
define('LOGGED_IN_SALT',   './ORtxpn4jCL(NpnA!g*&Jstte_D}=BhT5TBz^+cNz/x;%V|2!Va~HH@.-s<Z@D=');
define('NONCE_SALT',       'fJJ=UK,yo&l:.aOCaKXEC~!Y`Tj_)HR@:t#xmf8^|7Yg`0fk,Q>KZh&M3w}BT^%F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
